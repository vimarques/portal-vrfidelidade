import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  urlApi = "http://172.18.129.113:7771/fidelidade/";
  urlLumis = "http://dsp-v5.vr.com.br:8080/portal/lumis/api/rest/";

  constructor() { }
}
