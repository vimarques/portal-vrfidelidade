import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgxLoadingModule } from 'ngx-loading';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { PublicComponent } from './layout/public/public.component';
import { SecureComponent } from './layout/secure/secure.component';
import { UploadFileComponent } from './secure/upload-file/upload-file.component';
import { HomeComponent } from './secure/home/home.component';
import { LoginComponent } from './public/login/login.component';
import { HeaderComponent } from './layout/secure/header/header.component';
import { LeftMenuComponent } from './layout/secure/left-menu/left-menu.component';
import { AuthGuard } from 'src/common/auth.guard';
import { ConfirmModalComponent } from './modals/confirm-modal/confirm-modal.component';
import { MatSnackBarModule } from '@angular/material';
@NgModule({
  declarations: [
    AppComponent,
    PublicComponent,
    SecureComponent,
    UploadFileComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    LeftMenuComponent,
    ConfirmModalComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    NgxLoadingModule.forRoot({})
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
