import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from '../global/global.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private globalServ: GlobalService) { }

  login(user){
    let username = user.username;
    let password = user.password;
    return this.http.post<any>(`${this.globalServ.urlLumis}lumlogin?username=${username}&password=${password}`, null);

    // if((user.username == this.user) && (user.password == this.pass)){
    //   return true;
    // } else {
    //   return false;
    // }
  }
}
