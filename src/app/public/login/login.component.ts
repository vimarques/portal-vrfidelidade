import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  credencialsCorrect = true;
  constructor(private formBuilder: FormBuilder, public userServ: UserService, public router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('currentUser') == "true"){
      this.router.navigate(['/home']);
    }
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      this.userServ.login(this.loginForm.value).subscribe(data => {
        localStorage.setItem("currentUser", "true");
        localStorage.setItem("user", this.loginForm.value.username);
        this.router.navigate(['/home'], this.loginForm.value.username);
      }, error => {
        this.credencialsCorrect = false;
      })

      // let ok = this.userServ.login(this.loginForm.value);
      
      // if(ok){
      //   localStorage.setItem("currentUser", "true");
      //   this.router.navigate(['/home'], this.loginForm.value.username);
      // } else {
      //   this.credencialsCorrect = false;
      // }
  }

}
