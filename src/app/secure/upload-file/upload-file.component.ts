import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DocumentService } from 'src/app/services/document/document.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmModalComponent } from 'src/app/modals/confirm-modal/confirm-modal.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

const HttpUploadOptions = {
  headers: new HttpHeaders({ "Content-Type": "multipart/form-data" })
}
@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  tableForm: FormGroup;
  submitted = false;
  excellDocument;
  public loading = false;
  fileName: any;
  fileContent : any;
  fileExtension: any;
  fileExtensionError: boolean = true;

  constructor(
      private formBuilder: FormBuilder,
      public documentServ: DocumentService,
      public dialog: MatDialog,
      public http: HttpClient,
      private snackBar: MatSnackBar
    ) { }

  ngOnInit() {
    this.tableForm = this.formBuilder.group({
      document: ['', [Validators.required]]
    });
  }

  setImage(files:FileList){
    if(files.item(0) != null){
    this.excellDocument = files.item(0);
    this.fileName = this.excellDocument.name;
    var allowedExtensions = "csv";

    this.fileExtension = this.fileName.split('.').pop();
    console.log(this.excellDocument);
    if(this.fileExtension == allowedExtensions){
      this.fileExtensionError = true;
    } else {
      this.submitted = true;
      this.fileExtensionError = false;
    }
  } else {
    this.fileExtensionError = true;
  }
  }

  // convenience getter for easy access to form fields
  get f() { return this.tableForm.controls; }

  onSubmit(confirmed) {
    this.loading = true;
    // this.openDialog();

      this.submitted = true;

      // stop here if form is invalid
      if (this.tableForm.invalid || this.fileExtensionError == false) {
        this.loading = false;
        return;
      }

      if(confirmed == true){
        
      this.documentServ.uploadDocument(this.excellDocument, localStorage.getItem('user')).subscribe(
        data => {
          this.loading = false;
          this.snackBar.open("Upload de arquivo feito com sucesso", "close", {
            duration: 6000,
            panelClass: ['sucess-snackbar']
          });
          console.log(data);
        },
        err => {
          this.loading = false;
          this.snackBar.open(err.error.mensagem, "close", {
            duration: 6000,
            panelClass: ['error-snackbar']
          });
          console.log(err);
        }
      )
      } else {
        this.loading = false;
      }
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
    });

    dialogRef.afterClosed().subscribe(result => {
      this.onSubmit(result);
    });    
  }
}
