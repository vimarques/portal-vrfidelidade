import { TestBed } from '@angular/core/testing';

import { DocumentService } from './document.service';
import { HttpClientModule } from '@angular/common/http';

describe('DocumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      HttpClientModule
    ]
  }));
  

  it('should be created', () => {
    const service: DocumentService = TestBed.get(DocumentService);
    expect(service).toBeTruthy();
  });
});
