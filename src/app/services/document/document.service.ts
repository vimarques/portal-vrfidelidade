import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from '../global/global.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private http: HttpClient, private globalServ: GlobalService) { }

  uploadDocument(document: File, user: string){
    const formData:FormData = new FormData();

     formData.append('arquivo', document);

    return this.http.post<any>(`${this.globalServ.urlApi}arquivos/upload/pontosretroativos?nomeUsuario=${user}`, formData);
  }
}
